# Api cedula frontend

Esta es una aplicación de ejemplo creada con el framework angularjs, para consumir la api cedula.

La api consumida aqui 
[http://cedula-api.herokuapp.com/](http://cedula-api.herokuapp.com/)


## Inicio

Para empezar sólo tiene que clonar el  repositorio cedula-front e instalar las dependencias:

### Requisitos previos

Nodejs `node -v` [`https://nodejs.org/es/`](https://nodejs.org/es/)

Npm `npm -v`

### Clonar `cedula-front`

Clonar el repositorio `cedula-front` usando git:

```
git clone https://gitlab.com/richardjmora/cedula-front.git
cd cedula-front
```

Si lo que desea es iniciar un nuevo proyecto con otro nombre, puede hacer:

```
git clone --depth=1 https://gitlab.com/richardjmora/cedula-front.git <your-project-name>
```


### Instalar dependencias

```
npm install
```

### Ejecutar la aplicación


```
npm start
```

Ahora vaya a la aplicación [`localhost:8000`](localhost:8000).


## Contacto

Para obtener más información sobre la api Cedula favor echa un vistazo a [`http://cedula-api.herokuapp.com/`](http://cedula-api.herokuapp.com/).

